import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router, browserHistory } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './configureStore';
import App from './App';

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory} basename={'/'}>
            <App />
        </Router>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
