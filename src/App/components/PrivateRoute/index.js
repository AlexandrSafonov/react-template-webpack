import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect, withRouter } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, isLogged, ...rest }) => {
    console.log('test');
    return (
        <Route {...rest} render={props => {
            
            return (
                isLogged
                    ? <Component {...props} />
                    : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
            );
        }} />
    );
};

const mapStateToProps = ({ AuthorizationReducer }) => ({
    isLogged: AuthorizationReducer.isLogged
})

export default withRouter(connect(mapStateToProps)(PrivateRoute));