import React from 'react';
import PropTypes from 'prop-types';

import { NavLink } from 'react-router-dom';

const Menu = ({ items, isActive, className }) => {
    const getSubMenu = (sub_menu) => {
        return (
            <div className="sub-menu">
                <ul>
                    {sub_menu.map(item => getMenuItem(item))}
                </ul>
            </div>
        );
    };

    const getMenuItem = ({ id, link, title, sub_menu }) => {
        return (
            <li key={id}>
                <NavLink exact activeClassName="current-menu-item" to={link} title={title}>{title}</NavLink>
                {sub_menu && getSubMenu(sub_menu)}
            </li>
        );
    };

    return (
        <nav className={['menu', isActive ? 'show' : '', className].join(' ')}>
            <ul>
                {items.map(item => getMenuItem(item))}
            </ul>
        </nav>
    );
};

Menu.propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            link: PropTypes.string.isRequired,
            title: PropTypes.string,
            sub_menu: PropTypes.arrayOf(
                PropTypes.shape({
                    id: PropTypes.number.isRequired,
                    link: PropTypes.string.isRequired,
                    title: PropTypes.string,
                    sub_menu: PropTypes.array
                })
            )
        })
    ),
    className: PropTypes.string
};

Menu.defaultProps = {
    items: []
};

export default Menu;