import React from 'react';
import { connect } from 'react-redux';
import { logIn } from '../../application/authorization.services';

import Form from './Form';

const LogIn = ({ actions, state }) => (
    <div className="login-form">
        <Form {...actions} state={state} />
    </div>
);

const mapStateToProps = (state) => ({
    state: state
})

const mapDispatchToProps = (dispatch) => ({
    actions: {
        logIn: (user, callback) => dispatch(logIn(user, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(LogIn);