import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';

import Button from '../../Button';

class Form extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: ''
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const { username, password } = this.state;
        const { logIn } = this.props;

        logIn({ username, password });
    }

    onChange = (e) => {
        const value = e.currentTarget.value;
        const fieldName = e.currentTarget.name;

        this.setState({
            [fieldName]: value,
        });
    }

    render() {
        const { state: { AuthorizationReducer: { errorMsg, isLogged } } } = this.props;

        if (isLogged) {
            return <Redirect to={location.state ? location.state.from.pathname : '/'} />
        }

        return (
            <form onSubmit={this.onSubmit}>
                {errorMsg &&
                    <div>{errorMsg}</div>
                }
                <div className="input-wrap">
                    <input type="text" placeholder="User name" name="username" onChange={this.onChange} />
                </div>
                <div className="input-wrap">
                    <input type="password" placeholder="Password" name="password" onChange={this.onChange} />
                </div>
                <Button type="submit" className="blue">Sing In</Button>
            </form>
        );
    }
}

export default withRouter(Form);