import React from 'react';

const Button = ({ children, link = "/", title, type = "link", className, ...rest }) => {
    const getButton = () => {
        let btn = "";
        if (type == "link") {
            btn = <a className={['base-btn ', className].join()} href={link} title={title} {...rest}>{children}</a>;
        } else {
            btn = <button className={"base-btn " + className} type={type} {...rest}>{children}</button>
        }
        return btn;
    }

    return (
        <div className={"base-btn-wrap"}>
            {getButton()}
        </div>
    );
}

export default Button;