import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

const Logo = ({ link, title, image, className }) => (
    <Link className={['logo', className].join(' ')} to={link}>
        <img src={image} alt={title} title={title} />
    </Link>
);

Logo.propTypes = {
    link: PropTypes.string,
    title: PropTypes.string,
    image: PropTypes.string.isRequired,
    className: PropTypes.string
}

Logo.defaultProps = {
    link: "/"
};

export default Logo;