import React from 'react';
import { connect } from 'react-redux';
import { fetchHeaderNav } from '../../application/headerNav.services';
import { logOut } from '../../application/authorization.services';
import { withRouter } from 'react-router';

import Logo from '../Logo';
import Navigation from './Navigation';

const Header = ({ className, actions, navigation, browser, isLogged }) => (
    <header id="header" className={['header', className].join(' ')}>
        <div className="container">
            <div className="row align-items-center">
                <div className="col">
                    <Logo title="Site logo" image="/images/site/logo.png" />
                </div>
                <Navigation
                    className="col-auto"
                    {...actions}
                    navigation={navigation}
                    browser={browser}
                    isLogged={isLogged}
                />
            </div>
        </div>
    </header>
);

const mapStateToProps = ({ HeaderNavigationReducer, browser, AuthorizationReducer }) => ({
    navigation: HeaderNavigationReducer.navigation,
    browser: browser,
    isLogged: AuthorizationReducer.isLogged
});

const mapDispatchToProps = (dispatch) => ({
    actions: {
        fetchHeaderNav: () => dispatch(fetchHeaderNav()),
        logOut: () => dispatch(logOut())
    }
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));