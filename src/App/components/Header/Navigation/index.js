import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';

import Menu from '../../Menu';

export class Navigation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            menuState: false,
            isMobile: false
        }
    }

    componentDidMount() {
        this.props.fetchHeaderNav();
    }

    toggleMenu = () => {
        this.setState({
            menuState: !this.state.menuState
        });
    };

    handleLogOut = () => {
        this.props.logOut()
    }

    render() {
        const {className, navigation, browser, isLogged } = this.props;
        let isMobile = browser.lessThan.medium;
        
        return (
            <Fragment>
                {isMobile ? (
                    <Fragment>
                        <div className={[className].join(' ')}>
                            <button type="button" className="toggle-menu" title="Toggle menu" onClick={this.toggleMenu}>
                                <i className="fa fa-bars" aria-hidden="true"></i>
                            </button>
                        </div>
                        <Menu items={navigation} className="mobile-menu vertical" isActive={this.state.menuState} />
                    </Fragment>
                ) : (
                    <div className={[className].join(' ')}>
                        <Menu items={navigation} className="main-menu horizontal" />
                        {isLogged ?
                            <button type="button" onClick={this.handleLogOut}>LogOut</button>
                            :
                            <Link to="/login" title="LogIn">LogIn</Link>
                        }
                    </div>
                )}
            </Fragment>
        );
    }
}

export default Navigation;