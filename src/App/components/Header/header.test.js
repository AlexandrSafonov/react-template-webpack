import React from 'react';
import {shallow} from 'enzyme';

import { Navigation } from './Navigation/index';

describe('Header', () => {

    it('Mobile menu is render ', () => {
        const browser = { lessThan: { medium: true } }
        const wrapper = shallow(<Navigation navigation={[]} getNav={()=>{}} browser={browser} />);
        expect(wrapper.find('.mobile-menu')).toHaveLength(1);
    })

    it('Main menu is render', () => {
        const browser = { lessThan: { medium: false } }
        const wrapper = shallow(<Navigation navigation={[]} getNav={()=>{}} browser={browser} />);
        expect(wrapper.find('.main-menu')).toHaveLength(1);
    })

});