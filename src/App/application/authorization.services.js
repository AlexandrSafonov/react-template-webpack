import {
    LogIn,
    LogIsFailure,
    LogOut
} from './actions/authorization.actions';
import DATA from './DATA/users.json';

export const logIn = (user) => {
    let result = { state: false, message: 'Login not found' };
    let filterUser;

    DATA.forEach(({ id, login, password }) => {
        const reLogin = new RegExp('^' + login + '$'),
            rePassword = new RegExp('^' + password + '$'),
            wrongPass = { state: false, message: 'Password is wrong' },
            succes = { state: true };

        result = !reLogin.test(user.username) ? result : rePassword.test(user.password) ? succes : wrongPass;
        filterUser = reLogin.test(user.username) && rePassword.test(user.password) ? { "id": id, "login": login } : filterUser
    });

    if (result.state) {
        localStorage.setItem('user', JSON.stringify(filterUser));
        return LogIn(filterUser);
    } else {
        return LogIsFailure(result.message);
    }
}

export const logOut = () => {
    localStorage.removeItem('user');
    return LogOut();
}