import React from 'react';
import configureStore from 'redux-mock-store';
import getNav from './headerNav.services';

const mockStore = configureStore([])

jest.mock('./DATA/headerNav.json', () => [
    {
        "id": "1",
        "sub_menu": {
            "id": "1"
        }
    },
    {
        "id": "2",
        "sub_menu": {
            "id": "2"
        }
    },
    {
        "id": "3",
        "sub_menu": {
            "id": "3"
        }
    }
]);

describe('getNav', () => {
    beforeEach(() => {
        jest.resetAllMocks();
    });

    it('Should featch header navigation', () => {
        const store = mockStore();

        store.dispatch(getNav());

        expect(store.getActions()).toEqual([
            {
                type: 'SET_HEADER_NAVIGATION',
                payload: [
                    {
                        "id": "1",
                        "sub_menu": {
                            "id": "1"
                        }
                    },
                    {
                        "id": "2",
                        "sub_menu": {
                            "id": "2"
                        }
                    },
                    {
                        "id": "3",
                        "sub_menu": {
                            "id": "3"
                        }
                    }
                ]
            },
        ]);
    });

});