import {
    fetchHomePageBegin,
    fetchHomePageSuccess,
    fetchHomePageFailure
} from './actions/homePage.actions';

import DATA from './DATA/homePage.json';

export function fetchHomePage() {
    return dispatch => {
        let localData = new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(DATA);
            }, 500);
        });

        dispatch(fetchHomePageBegin());

        // fetch("https://api.github.com/users")    
        return localData
            // .then(handleErrors)
            // .then(res => res.json())
            .then(json => {
                dispatch(fetchHomePageSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchHomePageFailure(error)));
    };
}

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}