import {
    fetchHeaderNavigationBegin,
    fetchHeaderNavigationSuccess,
    fetchHeaderNavigationFailure
} from './actions/headerNav.actions';
import DATA from './DATA/headerNav';

export const fetchHeaderNav = () => (
    dispatch => {
        let promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(DATA);
            }, 500);
        });

        dispatch(fetchHeaderNavigationBegin());

        return promise.then(json => {
            dispatch(fetchHeaderNavigationSuccess(json));
            return json;
        });

        /*return fetch("https://api.github.com/users")
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchHeaderNavigationSuccess(json));
                return json;
            })
            .catch(error => dispatch(fetchHeaderNavigationFailure(error)));*/
    }
)

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}