export const FETCH_HEADER_NAVIGATION_BEGIN = 'FETCH_HEADER_NAVIGATION_BEGIN';
export const FETCH_HEADER_NAVIGATION_SUCCESS = 'FETCH_HEADER_NAVIGATION_SUCCESS';
export const FETCH_HEADER_NAVIGATION_FAILURE = 'FETCH_HEADER_NAVIGATION_FAILURE';

export const fetchHeaderNavigationBegin = () => ({
    type: FETCH_HEADER_NAVIGATION_BEGIN
});

export const fetchHeaderNavigationSuccess = (navigation) => ({
    type: FETCH_HEADER_NAVIGATION_SUCCESS,
    payload: { navigation }
});

export const fetchHeaderNavigationFailure = (error) => ({
    type: FETCH_HEADER_NAVIGATION_FAILURE,
    payload: { error }
});