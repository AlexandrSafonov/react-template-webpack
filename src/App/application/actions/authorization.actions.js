export const LOG_IN = 'LOG_IN';
export const LOG_IS_FAILURE = 'LOG_IS_FAILURE';
export const LOG_OUT = 'LOG_OUT';

export const LogIn = (user) => ({
    type: LOG_IN,
    payload: user
});

export const LogIsFailure = (message) => ({
    type: LOG_IS_FAILURE,
    payload: message
});

export const LogOut = () => ({
    type: LOG_OUT
});