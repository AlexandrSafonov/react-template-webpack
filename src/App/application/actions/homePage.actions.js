export const FETCH_HOMEPAGE_BEGIN   = 'FETCH_HOMEPAGE_BEGIN';
export const FETCH_HOMEPAGE_SUCCESS = 'FETCH_HOMEPAGE_SUCCESS';
export const FETCH_HOMEPAGE_FAILURE = 'FETCH_HOMEPAGE_FAILURE';

export const fetchHomePageBegin = () => ({
  type: FETCH_HOMEPAGE_BEGIN
});

export const fetchHomePageSuccess = content => ({
  type: FETCH_HOMEPAGE_SUCCESS,
  payload: { content }
});

export const fetchHomePageFailure = error => ({
  type: FETCH_HOMEPAGE_FAILURE,
  payload: { error }
});