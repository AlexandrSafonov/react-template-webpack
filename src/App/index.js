import React, { Component } from 'react';
import './scss/main.scss';

import { withRouter, Route, Switch } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import Header from './components/Header';
import PrivateRoute from './components/PrivateRoute';
import HomePage from './containers/HomePage';
import LoginPage from './containers/LoginPage';
import DefaultPage from './containers/DefaultPage';
import Page404 from './containers/Page404';
import AdminPage from './containers/AdminPage';

class App extends Component {
    render() {
        const { location } = this.props;

        return (
            <div id="main">
                <div id="main-content" className="main-content">
                    <Header />
                    <TransitionGroup>
                        <CSSTransition
                            key={location.key}
                            classNames="fade"
                            timeout={300}
                            appear={true}
                        >
                            <Switch>
                                <Route exact path="/" component={HomePage} />
                                <Route exact path="/pages/:slag" component={DefaultPage} />
                                <PrivateRoute exact path="/admin" component={AdminPage} />
                                <Route exact path="/login" component={LoginPage} />
                                <Route component={Page404} />
                            </Switch>
                        </CSSTransition>
                    </TransitionGroup>
                </div>
            </div>
        );
    }
}

export default withRouter(App);