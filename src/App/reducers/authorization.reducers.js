import {
    LOG_IN,
    LOG_IS_FAILURE,
    LOG_OUT
} from '../application/actions/authorization.actions';

let user = JSON.parse(localStorage.getItem('user'));

const initialState = user ? { isLogged: true, user } : {};

export default (state = initialState, action) => {
    switch (action.type) {
        case LOG_IN:
            return Object.assign({}, state, {
                isLogged: true,
                user: action.payload
            })
        case LOG_IS_FAILURE:
            return Object.assign({}, state, {
                errorMsg: action.payload,
            })
        case LOG_OUT:
            return {}
        default:
            return state;
    }
};