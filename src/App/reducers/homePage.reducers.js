import {
    FETCH_HOMEPAGE_BEGIN,
    FETCH_HOMEPAGE_SUCCESS,
    FETCH_HOMEPAGE_FAILURE
} from '../application/actions/homePage.actions';

const initialState = {
    content: {},
    loading: false,
    error: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_HOMEPAGE_BEGIN:
            return Object.assign({}, state, {
                loading: true,
                error: null
            })
        case FETCH_HOMEPAGE_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                content: action.payload.content
            })
        case FETCH_HOMEPAGE_FAILURE:
            return Object.assign({}, state, {
                loading: false,
                error: action.payload.error,
                content: []
            })
        default:
            return state
    }
} 