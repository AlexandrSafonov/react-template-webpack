import {
    FETCH_HEADER_NAVIGATION_BEGIN,
    FETCH_HEADER_NAVIGATION_SUCCESS,
    FETCH_HEADER_NAVIGATION_FAILURE
} from '../application/actions/headerNav.actions';

const initialState = {
    navigation: [],
    loading: false,
    error: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_HEADER_NAVIGATION_BEGIN:
            return Object.assign({}, state, {
                loading: true,
                error: null
            })
        case FETCH_HEADER_NAVIGATION_SUCCESS:
            return Object.assign({}, state, {
                loading: false,
                navigation: action.payload.navigation
            })
        case FETCH_HEADER_NAVIGATION_FAILURE:
            return Object.assign({}, state, {
                loading: false,
                error: action.payload.error,
                content: []
            })
        default:
            return state
    }
} 