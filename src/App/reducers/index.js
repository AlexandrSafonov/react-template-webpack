import HeaderNavigationReducer from './headerNav.reducers';
import AuthorizationReducer from './authorization.reducers';
import HomePageReducer from './homePage.reducers';
import { createResponsiveStateReducer } from 'redux-responsive';

export default {
    HeaderNavigationReducer,
    AuthorizationReducer,
    HomePageReducer,
    browser: createResponsiveStateReducer({
        extraSmall: 480,
        infinity: Infinity,
        large: 1200,
        medium: 992,
        small: 768
    })
};
