import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchHomePage } from '../../application/homePage.services';

class Home extends Component {
    componentDidMount() {
        this.props.dispatch(fetchHomePage())
    }

    render() {
        const { heroBanner = {}, about = {} } = this.props;

        return (
            <div className="content">
                <div className="content-section">
                    <div className="container">
                        {heroBanner.title}
                    </div>
                </div>
                <div className="content-section">
                    <div className="container">
                        {about.title}
                    </div>
                </div>
            </div>
        );
    }
};

const mapStateToProps = ({ HomePageReducer }) => ({
    heroBanner: HomePageReducer.content.hero_banner,
    about: HomePageReducer.content.about
})

export default connect(mapStateToProps)(Home);