import React, { Component } from 'react';
import { connect } from 'react-redux';

class DefaultPage extends Component {
    render() {
        const { match: { params: { slag } } } = this.props;
        return (
            <div className="content">
                <div className="content-section">
                    <div className="container">
                        {slag}
                    </div>
                </div>
            </div>
        );
    }
};

const mapStateToProps = ({ state }) => ({
    state: state
})

export default connect(mapStateToProps)(DefaultPage);