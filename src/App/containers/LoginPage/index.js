import React from 'react';

import Login from '../../components/Login';

const LoginPage = () => (
    <div id="main-content">
        <div className="content">
            <Login />
        </div>
    </div>
);

export default LoginPage;