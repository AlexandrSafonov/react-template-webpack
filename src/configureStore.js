import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { responsiveStoreEnhancer } from 'redux-responsive'
import { createLogger } from 'redux-logger';
import rootReducers from './App/reducers';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middlewares = [thunk, process.env.NODE_ENV === 'development' ? createLogger() : undefined].filter(
    middleware => !!middleware,
);

export default () => {
    const store = createStore(
        combineReducers({ ...rootReducers }),
        undefined,
        composeEnhancers(responsiveStoreEnhancer, applyMiddleware(...middlewares)),
    );

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('./App/reducers', () => {
            const nextRootReducer = require('./App/reducers');
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
};
