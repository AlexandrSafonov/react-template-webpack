const webpack = require('webpack');
const path = require("path");

const CleanPlugin = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin')

const DIST_DIR = path.resolve(__dirname, "dist");
const SRC_DIR = path.resolve(__dirname, "src");
const PUBLIC_DIR = path.resolve(__dirname, "public");

module.exports = function (env, argv) {
    console.log(`This is the Webpack 4 'mode': ${argv.mode}`);
    return {
        entry: {
            app: ["babel-polyfill", SRC_DIR + "/index.js"]
        },
        output: {
            path: DIST_DIR,
            filename: "bundle.js",
            publicPath: "/"
        },
        module: {
            rules: [
                {
                    test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot|ico|otf)$/,
                    loaders: ["file-loader"]
                },
                {
                    test: /\.css$/,
                    use: ["style-loader", "css-loader"]
                },
                {
                    test: /\.scss$/,
                    use: [
                        {
                            loader: argv.mode == "production" ? MiniCssExtractPlugin.loader : 'style-loader'
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                url: false
                            }
                        },
                        'postcss-loader',
                        'sass-loader'
                    ]
                },
                {
                    test: /\.js$/,
                    include: SRC_DIR,
                    loader: "babel-loader",
                    query: {
                        presets: ["react", "es2015", "stage-2"]
                    }
                }
            ]
        },
        performance: {
            maxEntrypointSize: 5000000,
            maxAssetSize: 5000000
        },
        devServer: {
            contentBase: PUBLIC_DIR,
            historyApiFallback: true,
            inline: true,
            hot: true,
            host: "localhost",
            port: "8080"
        },
        plugins: argv.mode == "production" ? [
            new CleanPlugin([DIST_DIR]),
            new MiniCssExtractPlugin({
                filename: "css/style.min.css"
            }),
            new CopyWebpackPlugin([
                { from: PUBLIC_DIR, to: DIST_DIR }
            ])
        ] : []
    }
};



